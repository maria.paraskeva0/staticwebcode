package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorTest 
{
	@Test
	public void test1()
	{
		final Calculator obj = new Calculator();
		assertEquals("Add", 9, obj.add());
	}

	@Test
        public void test2()
        {
                final Calculator obj = new Calculator();
                assertEquals("Sub", 3, obj.sub());
        }

}
