package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest
{
	@Test
	public void test1()
	{
		final Increment obj = new Increment();
		final int counter = obj.decreasecounter(0);
		assertEquals("test1", 1, counter);
	}

	@Test
        public void test2()
        {
                final Increment obj = new Increment();
                final int counter = obj.decreasecounter(1);
                assertEquals("test2", 1, counter);
        }

	@Test
        public void test3()
        {
                final Increment obj = new Increment();
                final int counter = obj.decreasecounter(obj.getCounter()+1);
                assertEquals("test3", 2, counter);
        }

}
