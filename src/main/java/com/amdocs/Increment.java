package com.amdocs;

public class Increment {
	
	private static int counter;

	public Increment() {
	   counter = 1;
	}
	public int getCounter() {
	    return counter++;
	}
			
	public int decreasecounter(final int input) {
		int res;
		if (input == 0) {
	       	  res = counter--;
		}
		else if (input == 1) {
		     res = counter;
		}
		else {
		  res = counter++;
		}
		return res;
	}			
}
